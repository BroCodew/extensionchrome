import React, { useEffect, useState } from "react";

const Test = () => {
  console.log("33333");
  const [deleteSuccess, setDeleteSuccess] = useState(false);

  useEffect(() => {
      // Lắng nghe thông điệp từ mã nền
      chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
          if (message.action === "DeleteSuccess") {
              setDeleteSuccess(true);

              // Sau một khoảng thời gian, đặt trạng thái thành false để ẩn thông báo
              setTimeout(() => {
                  setDeleteSuccess(false);
              }, 5000); // 5000 là thời gian hiển thị thông báo (5 giây)
          }
      });
  }, []);

  // content.js


  const handleExport = () => {
    chrome.runtime.sendMessage({ action: "Export" }, function (response) {
      if (response.success) {
        const cookieString = response.cookieString;
  
        // Sử dụng Clipboard API để sao chép nội dung vào clipboard
        navigator.clipboard.writeText(cookieString)
          .then(() => {
            // Sao chép thành công
            alert("Cookies đã được sao chép vào clipboard!");
          })
          .catch((error) => {
            console.error("Lỗi khi sao chép vào clipboard:", error);
            alert("Lỗi khi sao chép vào clipboard.");
          });
      }
    });
  };
  
  const handleDelete = (e: any) => {
    chrome.runtime.sendMessage({ action: "Delete" }, function (response) {
      console.log(response);
    });
  };

  return (
    <div>
    {/* Hiển thị thông báo khi xóa thành công */}
        {deleteSuccess && <div>Deleted successfully!</div>}
        <button onClick={handleExport}>Copy Cookies</button>
        <button onClick={handleDelete}>Delete Cookies</button>

    
    </div>
  );
};

export default Test;
